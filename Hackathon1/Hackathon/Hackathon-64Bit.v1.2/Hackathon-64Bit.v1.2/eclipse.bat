rem *******************************************************************************
rem Variables
set SOFTWARE_PATH=%CD%
set WORKSPACE_PATH=%CD%\workspace
set ECLIPSE_HOME=%SOFTWARE_PATH%\eclipse
rem *******************************************************************************

rem ********************************************************************************
rem Java
set JAVA_HOME=%SOFTWARE_PATH%\Java\jdk1.8.0_92
rem set JAVA_OPTS=-Dhttp.proxyHost=myproxy.com -Dhttp.proxyPort=8080

rem ********************************************************************************
rem Eclipse
set ECLIPSE_OPT=-vm %JAVA_HOME%\bin\javaw -showlocation %WORKSPACE% -vmargs %ECLIPSE_VMARGS%

cd %ECLIPSE_HOME%
start /min eclipse.exe -data %WORKSPACE_PATH% %ECLIPSE_OPT%