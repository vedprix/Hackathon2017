package com.capgemini.mqtt;


import java.util.logging.Logger;

import javax.servlet.ServletContext;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import com.ibm.iotf.client.AbstractClient;

/**
 * 
 * @author cmammado
 *
 */
public class AgentListener implements ServletContextListener {
	
	private static Agent agent;
	// Path of the graphhopper folder
	public static String graphhopperPath;
	
	private static final String CLASS_NAME = AbstractClient.class.getName();
	private static final Logger LOG = Logger.getLogger(CLASS_NAME);

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		LOG.info("contextDestroyed");
		getAgent().disconnect();

	}

	@Override
	public void contextInitialized(ServletContextEvent servletContextEvent) {
		LOG.info("contextInitialized");
		ServletContext context = servletContextEvent.getServletContext();

		// Get the path of file application.prop
		String appPropPath = context.getRealPath("application.prop");
		agent = new Agent(appPropPath);

		// Get the path of the folder containing graphhopper files
		graphhopperPath = context.getRealPath("graphhopper");
		
	}

	public static Agent getAgent() {
		return agent;
	}

}
